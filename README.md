## S3 Database Backup

This module provides a database backup solution and exports to
the AWS S3 storage system.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/s3_db_backup


* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/s3_db_backup

### Features

* Keeps a history of database backups and can be accessed via
interface.
* Allows backups to be exported to AWS S3 storage system.
* Allows for configurable endpoint in AWS S3 storage system.
* Allows for sub-folders in AWS S3 storage system.
* Allows backups to be stored in private or public file system.
Private is recommended.
* Allows backups to be downloaded remotely via interface.
* Backups can be generated via Drupal cron and interval can be
configured.
* Drush command available to help generate database backups via
cli.

### Backup Features

* Supports sqlite, mysql, pgsql and dblib.
* Supports compressions gzip and bzip2.
* Supports a large option set as found in mysqldump.
* Supports stored procedures.
* Select which tables are included or excluded on export.

### Requirements

* AWS S3 account - (https://aws.amazon.com/s3/)
* AWS S3 Key & Secret Key

### Composer Dependencies

This module depends upon Composer to install (it's dependencies).

* mysqldump-php library - (https://github.com/ifsnop/mysqldump-php)
* aws-sdk-php library - (https://github.com/aws/aws-sdk-php)

### Module Dependencies

* Key module (https://www.drupal.org/project/key)
* Key AWS S3 module (https://www.drupal.org/project/key_aws_s3)

### AWS S3 Keys

AWS S3 access and secret keys are managed through the use of the key module.

More information about the key module can be found here:
https://www.drupal.org/project/key

Keys can be setup and managed here: **/admin/config/system/keys**

Once keys are setup, you can link them up here with the S3 database backup
module: **/admin/config/s3-db-backup/settings**

### Installation/Usage

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information. Installing
through composer will pull in all required dependencies.

```bash
composer require drupal/s3_db_backup
```

* Setup access & secret keys here: **/admin/config/system/keys**

* Once installed, setup configuration and AWS S3 at:
**/admin/config/s3-db-backup/settings**

* If storing or exporting to local, please setup or use private directory
instead of public. Defaults to public directory at the moment.

* Now you can run an export: **/admin/config/s3-db-backup**

### Drush Commands

Once module is configured and setup. Database backups/exports can be generated
via drush command. Please keep in mind that the download links are only made
available in Drupal admin backend.

```bash
drush s3-db-backup:export
```

### Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders

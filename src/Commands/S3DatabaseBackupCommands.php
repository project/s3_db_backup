<?php

namespace Drupal\s3_db_backup\Commands;

use Drupal\s3_db_backup\S3DatabaseBackup;
use Drupal\s3_db_backup\Adapter\S3DatabaseAWSBackupAdapter;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Helper\Table;

/**
 * S3 Database Backup Commands.
 */
class S3DatabaseBackupCommands extends DrushCommands {

  /**
   * S3 Database Backup Service.
   *
   * @var \Drupal\s3_db_backup\S3DatabaseBackup
   */
  protected $databaseBackup;

  /**
   * S3 Database AWS Backup Adapter.
   *
   * @var \Drupal\s3_db_backup\Adapter\S3DatabaseAWSBackupAdapter
   */
  protected $databaseAWSBackupAdapter;

  /**
   * Constructor.
   *
   * @param \Drupal\s3_db_backup\S3DatabaseBackup $databaseBackup
   *   S3 database backup object.
   * @param \Drupal\s3_db_backup\Adapter\S3DatabaseAWSBackupAdapter $databaseAWSBackupAdapter
   *   AWS database backup adapter.
   */
  public function __construct(S3DatabaseBackup $databaseBackup, S3DatabaseAWSBackupAdapter $databaseAWSBackupAdapter) {
    parent::__construct();
    $this->databaseBackup = $databaseBackup;
    $this->databaseAWSBackupAdapter = $databaseAWSBackupAdapter;
  }

  /**
   * S3 Database Backup.
   *
   * @command s3-db-backup:export
   * @aliases s3dbb
   *
   * @usage s3-db-backup:export
   */
  public function export() {
    $this->io()->writeln('S3 database backup starting...');

    // Get entry id.
    if ($eid = $this->databaseAWSBackupAdapter->export()) {

      // Pull up export entry record.
      $exportEntry = $this->databaseBackup->loadHistoryEntry($eid);

      // Make sure object key is set.
      if ($exportEntry->s3_object_key) {

        // Setup table for output.
        $headers = [
          'Export Id',
          'File Id',
          'Name',
          'Created',
        ];

        // Setup rows.
        $rows = [];
        $rows[] = [
          $exportEntry->eid,
          $exportEntry->fid,
          $exportEntry->name,
          $exportEntry->created,
        ];

        // Render table.
        $table = new Table($this->output);
        $table->setHeaders($headers)->setRows($rows);
        $table->render();

        // Success message.
        $this->io()->success('Database export generated successfully. Please login to admin to get access to download links.');
      }
      else {
        $this->io()->error('S3 object key not found.');
      }

    }
    else {
      $this->io()->error('Database backup export failed or entry not found.');
    }
  }

}

<?php

namespace Drupal\s3_db_backup;

use Ifsnop\Mysqldump\Mysqldump;
use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * S3 Database Backup Client.
 */
class S3DatabaseBackupClient implements S3DatabaseBackupClientInterface {

  /**
   * S3 Database Backup File Handler.
   *
   * @var \Drupal\s3_db_backup\S3DatabaseBackupFileHandler
   */
  protected $handler;

  /**
   * Database Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Constructor.
   *
   * @param \Drupal\s3_db_backup\S3DatabaseBackupFileHandler $handler
   *   S3 database backup file handler object.
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection object.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Config factory object.
   */
  public function __construct(S3DatabaseBackupFileHandler $handler, Connection $connection, ConfigFactoryInterface $config) {
    $this->handler = $handler;
    $this->connection = $connection;
    $this->config = $config;
  }

  /**
   * Perform the database dump.
   *
   * @return \Ifsnop\Mysqldump\Mysqldump
   *   Returns Mysqldump object.
   *
   * @throws \Exception
   */
  public function dump(): Mysqldump {
    // Get configuration for export.
    $config = $this->getSettings();
    $options = $this->getConnectionOptions();

    // Return export to the adapter.
    return new Mysqldump(
      $options['connection'],
      $options['username'],
      $options['password'],
      [
        'include-tables' => $config->get('settings.include_tables'),
        'exclude-tables' => $config->get('settings.exclude_tables'),
        'compress' => $config->get('settings.compress'),
        'no-data' => $config->get('settings.no_data'),
        'add-drop-table' => $config->get('settings.add_drop_table'),
        'single-transaction' => $config->get('settings.single_transaction'),
        'lock-tables' => $config->get('settings.lock_tables'),
        'add-locks' => $config->get('settings.add_locks'),
        'extended-insert' => $config->get('settings.extended_insert'),
        'complete-insert' => $config->get('settings.complete_insert'),
        'disable-keys' => $config->get('settings.disable_keys'),
        'where' => $config->get('settings.where'),
        'no-create-info' => $config->get('settings.no_create_info'),
        'skip-triggers' => $config->get('settings.skip_triggers'),
        'add-drop-trigger' => $config->get('settings.add_drop_trigger'),
        'routines' => $config->get('settings.routines'),
        'hex-blob' => $config->get('settings.hex_blob'),
        'databases' => $config->get('settings.databases'),
        'add-drop-database' => $config->get('settings.add_drop_database'),
        'skip-tz-utc' => $config->get('settings.skip_tz_utc'),
        'no-autocommit' => $config->get('settings.no_autocommit'),
        'default-character-set' => $config->get('settings.default_character_set'),
        'skip-comments' => $config->get('settings.skip_comments'),
        'skip-dump-date' => $config->get('settings.skip_dump_date'),
      ]
    );
  }

  /**
   * Get connection options.
   *
   * @return array
   *   Returns connection options.
   */
  public function getConnectionOptions(): array {
    $options = $this->connection->getConnectionOptions();

    // Requires formatting as a string.
    $connection = $options['driver'] . ':host=';
    $connection .= $options['host'] . ';port=';
    $connection .= $options['port'] . ';dbname=';
    $connection .= $options['database'];

    return [
      'connection' => $connection,
      'username' => $options['username'],
      'password' => $options['password'],
    ];
  }

  /**
   * Get database connection.
   *
   * @return \Drupal\Core\Database\Connection
   *   Returns database connection.
   */
  public function getConnection(): Connection {
    return $this->connection;
  }

  /**
   * Set or update the current database connection.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection object.
   */
  public function setConnection(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * Get file handler.
   *
   * @return \Drupal\s3_db_backup\S3DatabaseBackupFileHandler
   *   Returns the S3 database backup file handler.
   */
  public function getFileHandler(): S3DatabaseBackupFileHandler {
    return $this->handler;
  }

  /**
   * Get config settings.
   *
   * @param string $name
   *   Config settings name.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   Returns config object.
   */
  public function getSettings(string $name = 's3_db_backup.settings'): ImmutableConfig {
    return $this->config->get($name);
  }

}

<?php

namespace Drupal\s3_db_backup;

use Drupal\Core\Config\Config;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelTrait;

/**
 * S3 Database Backup File Handler.
 */
class S3DatabaseBackupFileHandler {

  use LoggerChannelTrait;

  /**
   * S3 Database Backup File Service.
   *
   * @var \Drupal\s3_db_backup\S3DatabaseBackupFile
   */
  protected $file;

  /**
   * Filesystem service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $filesystem;

  /**
   * File config.
   *
   * @var array
   */
  protected $config;

  /**
   * Date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Constructor.
   *
   * @param \Drupal\s3_db_backup\S3DatabaseBackupFile $file
   *   S3 database backup file object.
   * @param \Drupal\Core\File\FileSystem $filesystem
   *   Filesystem object.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   Date formatter service object.
   */
  public function __construct(S3DatabaseBackupFile $file, FileSystem $filesystem, DateFormatter $dateFormatter) {
    $this->file = $file;
    $this->filesystem = $filesystem;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * Handles file generation & setup.
   *
   * @param \Drupal\Core\Config\Config $config
   *   Configuration for file.
   */
  public function setupFile(Config $config) {
    $this->setConfig($config);

    $path = $this->createFilePath();
    $extension = $this->createFileType();

    $this->file->setFileName($this->config['name'] . '_' . time() . $extension);
    $this->file->setFileUri($this->filesystem->createFilename($this->file->getFileName(), $path));
  }

  /**
   * Get the file.
   *
   * @return \Drupal\s3_db_backup\S3DatabaseBackupFile
   *   Returns S3 database backup file object.
   */
  public function getFile(): S3DatabaseBackupFile {
    return $this->file;
  }

  /**
   * Set file.
   *
   * @param \Drupal\s3_db_backup\S3DatabaseBackupFile $file
   *   S3 database backup file object.
   */
  public function setFile(S3DatabaseBackupFile $file) {
    $this->file = $file;
  }

  /**
   * Set default file info.
   *
   * @param \Drupal\Core\Config\Config $config
   *   Configuration object.
   */
  private function setConfig(Config $config) {
    $this->config = [
      'date' => $this->dateFormatter->format(time(), $config->get('date')),
      'compress' => $config->get('settings.compress'),
      'name' => $config->get('filename'),
      'path' => $config->get('path'),
    ];
  }

  /**
   * Create export location.
   *
   * @return false|mixed|string
   *   Returns filepath or false for failure.
   */
  private function createFilePath() {
    $result = $this->config['path'];

    if (!$this->filesystem->prepareDirectory($result, FileSystemInterface::CREATE_DIRECTORY)) {
      $result = FALSE;
      $this->getLogger('s3_db_backup')->error('The requested directory @dir could not be created.', ['@dir' => $this->config['path']]);
    }
    else {
      if (!$this->filesystem->prepareDirectory($result)) {
        $result = FALSE;
        $this->getLogger('s3_db_backup')->error('The requested directory @dir permissions are not writable.', ['@dir' => $this->config['path']]);
      }
    }

    if ($this->config['date']) {
      $filepath = $this->config['path'] . '/' . $this->config['date'];
      if ($this->filesystem->prepareDirectory($filepath, FileSystemInterface::CREATE_DIRECTORY)) {
        $result = $filepath;
      }
    }

    return $result;
  }

  /**
   * Determine the export type & extension.
   *
   * @return string
   *   Returns file extension.
   */
  private function createFileType(): string {
    $type = 'application/octet-stream';
    $extension = '.sql';

    switch ($this->config['compress']) {
      case 'Gzip':
        $extension .= '.gz';
        $type = 'application/gzip';
        break;

      case 'Bzip2':
        $extension .= '.bz2';
        $type = 'application/x-bzip2';
        break;
    }

    $this->file->setFileType($type);
    return $extension;
  }

}

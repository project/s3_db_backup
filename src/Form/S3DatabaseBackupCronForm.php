<?php

namespace Drupal\s3_db_backup\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\State\StateInterface;
use Drupal\s3_db_backup\S3DatabaseBackup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * S3 Database Backup Cron Form.
 */
class S3DatabaseBackupCronForm extends ConfigFormBase {

  /**
   * S3 Database Backup Service.
   *
   * @var \Drupal\s3_db_backup\S3DatabaseBackup
   */
  protected $databaseBackup;

  /**
   * Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Stage storage service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory object.
   * @param \Drupal\s3_db_backup\S3DatabaseBackup $databaseBackup
   *   S3 database backup object.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time service object.
   * @param \Drupal\Core\State\StateInterface $state
   *   State storage service object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, S3DatabaseBackup $databaseBackup, TimeInterface $time, StateInterface $state) {
    parent::__construct($config_factory);
    $this->databaseBackup = $databaseBackup;
    $this->time = $time;
    $this->state = $state;
  }

  /**
   * Service injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container object.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('s3_db_backup'),
      $container->get('datetime.time'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 's3_db_backup_cron';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['s3_db_backup.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('s3_db_backup.settings');

    $form['cron'] = [
      '#type' => 'details',
      '#title' => $this->t('Cron Backup'),
      '#open' => TRUE,
    ];

    $form['cron']['cron_backup_enabled'] = [
      '#title' => $this->t('Enable database backups on cron runs.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('cron_backup_enabled'),
    ];

    $form['cron']['cron_interval'] = [
      '#title' => $this->t('Backup Interval (hours)'),
      '#type' => 'number',
      '#step' => '1',
      '#min' => 0,
      '#description' => $this->t('Number of hours to wait between backups.'),
      '#required' => TRUE,
      '#default_value' => $config->get('cron_interval'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Check and make sure cron interval value is acceptable.
    if ($form_state->getValue('cron_interval') <= 0) {
      $form_state->setErrorByName('cron_interval', $this->t('Cron interval should be greater than 0 hours.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('s3_db_backup.settings')
      ->set('cron_interval', $form_state->getValue('cron_interval'))
      ->set('cron_backup_enabled', $form_state->getValue('cron_backup_enabled'))
      ->save();

    $request_time = $this->time->getRequestTime();
    $this->state->set('s3_db_backup.cron_next_backup', $request_time + ($form_state->getValue('cron_interval') * 60));
  }

}

<?php

namespace Drupal\s3_db_backup\Form;

use Drupal\s3_db_backup\S3DatabaseBackup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * S3 Database Backup Tables Form.
 */
class S3DatabaseBackupTablesForm extends ConfigFormBase {

  /**
   * S3 Database Backup Service.
   *
   * @var \Drupal\s3_db_backup\S3DatabaseBackup
   */
  protected $databaseBackup;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory object.
   * @param \Drupal\s3_db_backup\S3DatabaseBackup $databaseBackup
   *   S3 database backup object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, S3DatabaseBackup $databaseBackup) {
    parent::__construct($config_factory);
    $this->databaseBackup = $databaseBackup;
  }

  /**
   * Service injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container object.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('s3_db_backup')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 's3_db_backup_table';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['s3_db_backup.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('s3_db_backup.settings');
    $tables = $this->databaseBackup->showTables();
    $options = $this->databaseBackup->formatOptions($tables);

    $form['tables'] = [
      '#type' => 'details',
      '#title' => $this->t('Database Tables'),
      '#open' => TRUE,
    ];

    $form['tables']['include_tables'] = [
      '#type' => 'select',
      '#title' => $this->t('Include tables'),
      '#options' => $options,
      '#multiple' => TRUE,
      '#attributes' => [
        'size' => '8',
      ],
      '#description' => $this->t('Assign tables to include, leave empty for all.'),
      '#default_value' => $config->get('settings.include_tables'),
    ];

    $form['tables']['exclude_tables'] = [
      '#type' => 'select',
      '#title' => $this->t('Exclude tables'),
      '#options' => $options,
      '#multiple' => TRUE,
      '#attributes' => [
        'size' => '8',
      ],
      '#description' => $this->t('Assign tables to exclude, leave empty for none.'),
      '#default_value' => $config->get('settings.exclude_tables'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Save configuration.
    $this->config('s3_db_backup.settings')
      ->set('settings.include_tables', $form_state->getValue('include_tables'))
      ->set('settings.exclude_tables', $form_state->getValue('exclude_tables'))
      ->save();
  }

}

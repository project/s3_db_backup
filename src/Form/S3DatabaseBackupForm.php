<?php

namespace Drupal\s3_db_backup\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\s3_db_backup\S3DatabaseBackup;
use Drupal\s3_db_backup\S3DatabaseBackupClient;
use Drupal\s3_db_backup\Adapter\S3DatabaseLocalBackupAdapter;
use Drupal\s3_db_backup\Adapter\S3DatabaseRemoteBackupAdapter;
use Drupal\s3_db_backup\Adapter\S3DatabaseAWSBackupAdapter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * S3 Database Backup Form.
 */
class S3DatabaseBackupForm extends FormBase {

  /**
   * S3 Database Backup Client Service.
   *
   * @var \Drupal\s3_db_backup\S3DatabaseBackupClient
   */
  protected $databaseBackupClient;

  /**
   * S3 Database Backup Service.
   *
   * @var \Drupal\s3_db_backup\S3DatabaseBackup
   */
  protected $databaseBackup;

  /**
   * S3 Database AWS Backup Adapter.
   *
   * @var \Drupal\s3_db_backup\Adapter\S3DatabaseAWSBackupAdapter
   */
  protected $databaseAWSBackupAdapter;

  /**
   * S3 Database Local Backup Adapter.
   *
   * @var \Drupal\s3_db_backup\Adapter\S3DatabaseLocalBackupAdapter
   */
  protected $databaseLocalBackupAdapter;

  /**
   * S3 Database Remote Backup Adapter.
   *
   * @var \Drupal\s3_db_backup\Adapter\S3DatabaseRemoteBackupAdapter
   */
  protected $databaseRemoteBackupAdapter;

  /**
   * Constructor.
   *
   * @param \Drupal\s3_db_backup\S3DatabaseBackupClient $databaseBackupClient
   *   S3 database backup client object.
   * @param \Drupal\s3_db_backup\S3DatabaseBackup $databaseBackup
   *   S3 database backup object.
   * @param \Drupal\s3_db_backup\Adapter\S3DatabaseAWSBackupAdapter $databaseAWSBackupAdapter
   *   S3 database AWS backup adapter.
   * @param \Drupal\s3_db_backup\Adapter\S3DatabaseLocalBackupAdapter $databaseLocalBackupAdapter
   *   S3 database local backup adapter.
   * @param \Drupal\s3_db_backup\Adapter\S3DatabaseRemoteBackupAdapter $databaseRemoteBackupAdapter
   *   S3 database remote backup adapter.
   */
  public function __construct(
    S3DatabaseBackupClient $databaseBackupClient,
    S3DatabaseBackup $databaseBackup,
    S3DatabaseAWSBackupAdapter $databaseAWSBackupAdapter,
    S3DatabaseLocalBackupAdapter $databaseLocalBackupAdapter,
    S3DatabaseRemoteBackupAdapter $databaseRemoteBackupAdapter
  ) {
    $this->databaseBackupClient = $databaseBackupClient;
    $this->databaseBackup = $databaseBackup;
    $this->databaseAWSBackupAdapter = $databaseAWSBackupAdapter;
    $this->databaseLocalBackupAdapter = $databaseLocalBackupAdapter;
    $this->databaseRemoteBackupAdapter = $databaseRemoteBackupAdapter;
  }

  /**
   * Service injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container object.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('s3_db_backup.client'),
      $container->get('s3_db_backup'),
      $container->get('s3_db_backup.adapter.aws'),
      $container->get('s3_db_backup.adapter.local'),
      $container->get('s3_db_backup.adapter.remote'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 's3_db_backup_export';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('s3_db_backup.settings');
    $site_name = $this->config('system.site')->get('name');

    // Warn user that AWS S3 is not configured yet.
    if (!$config->get('s3.status')) {
      $this->messenger()
        ->addMessage($this->t('AWS S3 export option not available. Please configure credentials for this export type under settings tab.'), 'warning');
    }

    $form['backup'] = [
      '#type' => 'details',
      '#title' => $this->t('Database Backup'),
      '#open' => TRUE,
    ];

    $form['backup']['filename'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Filename'),
      '#description' => $this->t('The prefix name of the sql dump file.'),
      '#default_value' => $config->get('filename') ? $config->get('filename') : $site_name,
      '#field_suffix' => '.sql',
      '#size' => 40,
    ];
    if ($config->get('settings.compress') == 'Gzip') {
      $form['backup']['filename']['#field_suffix'] = '.sql.gz';
    }
    if ($config->get('settings.compress') == 'Bzip2') {
      $form['backup']['filename']['#field_suffix'] = '.sql.bz2';
    }

    $defaultExportType = 'local';
    $exportTypes = [
      'local' => $this->t('Local'),
      'download' => $this->t('Download'),
    ];
    if ($config->get('s3.status')) {
      $defaultExportType = 'aws';
      $exportTypes['aws'] = $this->t('AWS S3 Storage');
    }
    $form['backup']['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Export type'),
      '#options' => $exportTypes,
      '#description' => $this->t('Export database backup to local, download or AWS S3.'),
      '#default_value' => $defaultExportType,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export Database'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Save filename.
    $this->databaseBackup->getConfig()
      ->set('filename', $values['filename'])
      ->save();

    // Select our adapter.
    switch ($values['type']) {

      case 'download':
        $adapter = $this->databaseRemoteBackupAdapter;
        break;

      case 'aws':
        $adapter = $this->databaseAWSBackupAdapter;
        break;

      default:
        $adapter = $this->databaseLocalBackupAdapter;
        break;
    }

    // Run the export.
    if ($adapter && $adapter->export()) {
      $this->messenger()
        ->addMessage($this->t('Database backup has been successfully completed.'), 'status');
      $form_state->setRedirect('s3_db_backup.export_history');
    }
    else {
      $this->messenger()
        ->addMessage($this->t('Database backup has failed, please review recent log messages.'), 'warning');
    }
  }

}

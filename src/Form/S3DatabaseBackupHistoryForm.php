<?php

namespace Drupal\s3_db_backup\Form;

use Drupal\s3_db_backup\S3DatabaseBackup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Aws\S3\Exception\S3Exception;

/**
 * S3 Database Backup History Form.
 */
class S3DatabaseBackupHistoryForm extends FormBase {

  /**
   * S3 Database Backup Service.
   *
   * @var \Drupal\s3_db_backup\S3DatabaseBackup
   */
  protected $databaseBackup;

  /**
   * Constructor.
   *
   * @param \Drupal\s3_db_backup\S3DatabaseBackup $databaseBackup
   *   S3 database backup object.
   */
  public function __construct(S3DatabaseBackup $databaseBackup) {
    $this->databaseBackup = $databaseBackup;
  }

  /**
   * Service injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container object.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('s3_db_backup')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 's3_db_backup_history';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $rows = []): array {
    $header = [
      'eid' => $this->t('Export Id'),
      'fid' => $this->t('File Id'),
      'name' => $this->t('Filename'),
      'size' => $this->t('File Size'),
      'uri' => $this->t('Local File'),
      's3_object_uri' => $this->t('AWS S3 File'),
      'created' => $this->t('Created'),
    ];

    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $rows,
      '#empty' => $this->t('No database export history found.'),
    ];

    if ($rows) {
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Delete Export(s)'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->databaseBackup->getConfig();
    $values = $form_state->getValues();

    // Loop over files that have been selected.
    $files = array_filter($values['table']);
    foreach ($files as $fid) {

      // Load up export entry.
      $exportEntry = $this->databaseBackup->loadHistoryEntryByFileId($fid);
      if ($exportEntry) {

        try {
          // Make sure we have object key.
          if ($exportEntry->s3_object_key) {

            // Make sure we have a s3 client.
            if ($s3 = $this->databaseBackup->getS3Client()) {

              // Delete object from s3.
              $s3->deleteObject([
                'Bucket' => $config->get('s3.bucket'),
                'Key' => $exportEntry->s3_object_key,
              ]);
            }
          }

          // Remove history entry and file.
          $this->databaseBackup->deleteHistoryEntry($fid);
        }
        catch (S3Exception | \Exception $e) {
          watchdog_exception('s3_db_backup', $e);
        }
      }
    }

    $this->messenger()->addMessage($this->t('Database backup(s) has been removed successfully.'));
  }

}

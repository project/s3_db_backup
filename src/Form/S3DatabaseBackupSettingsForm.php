<?php

namespace Drupal\s3_db_backup\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\s3_db_backup\S3DatabaseBackup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\StreamWrapper\PrivateStream;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * S3 Database Backup Settings Form.
 */
class S3DatabaseBackupSettingsForm extends ConfigFormBase {

  /**
   * S3 Database Backup Service.
   *
   * @var \Drupal\s3_db_backup\S3DatabaseBackup
   */
  protected $databaseBackup;

  /**
   * Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory object.
   * @param \Drupal\s3_db_backup\S3DatabaseBackup $databaseBackup
   *   S3 database backup object.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time service object.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   Date formatter service object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, S3DatabaseBackup $databaseBackup, TimeInterface $time, DateFormatter $dateFormatter, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($config_factory);
    $this->databaseBackup = $databaseBackup;
    $this->time = $time;
    $this->dateFormatter = $dateFormatter;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Service injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container object.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('s3_db_backup'),
      $container->get('datetime.time'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 's3_db_backup_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['s3_db_backup.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('s3_db_backup.settings');
    $request_time = $this->time->getRequestTime();

    // Warn user that backups should be stored in private directory.
    if (!PrivateStream::basePath()) {
      $this->messenger()
        ->addMessage($this->t('Private directory is not set and should be used for backup storage.'), 'warning');
    }

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General'),
      '#open' => TRUE,
    ];

    $form['general']['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path'),
      '#description' => $this->t('The path database backups are saved to, should be a URI.'),
      '#default_value' => $config->get('path'),
    ];

    $date_types = $this->entityTypeManager->getStorage('date_format')
      ->loadMultiple();
    $date_format_options = [
      '' => $this->t('- Select Date Format -'),
    ];
    foreach ($date_types as $machine_name => $format) {
      $date_format_options[$machine_name] = $this->t('@name - @sample', [
        '@name' => $format->label(),
        '@sample' => $this->dateFormatter->format($request_time, $machine_name),
      ]);
    }
    $form['general']['date'] = [
      '#type' => 'select',
      '#title' => $this->t('Date Format'),
      '#options' => $date_format_options,
      '#description' => $this->t('Creates sub folders inside path with date format name.'),
      '#default_value' => $config->get('date'),
    ];

    $form['general']['compress'] = [
      '#type' => 'select',
      '#title' => $this->t('Compression Type'),
      '#options' => [
        'None' => $this->t('None'),
        'Gzip' => $this->t('Gzip'),
        'Bzip2' => $this->t('Bzip2'),
      ],
      '#description' => $this->t('Compress the database export.'),
      '#default_value' => $config->get('settings.compress'),
    ];

    $form['general']['page_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Paging Limit'),
      '#min' => 10,
      '#description' => $this->t('Set paging limit for backup export history results.'),
      '#default_value' => $config->get('page_limit') ?? 10,
    ];

    $form['s3'] = [
      '#type' => 'details',
      '#title' => $this->t('AWS S3'),
      '#open' => $config->get('s3.status'),
    ];

    $form['s3']['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Toggle AWS S3'),
      '#description' => $this->t('Enable or disable AWS S3 export service.'),
      '#default_value' => $config->get('s3.status'),
    ];

    $form['s3']['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('S3 Endpoint/Host'),
      '#description' => $this->t('Enter Host/Endpoint name. For e.g. <i>https://s3.amazonaws.com</i>'),
      '#default_value' => $config->get('s3.endpoint'),
      '#states' => [
        'visible' => [
          ':input[name="status"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $key_collection_url = Url::fromRoute('entity.key.collection')->toString();
    $form['s3']['keys'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Keys'),
      '#description' => $this->t('Use keys managed by the key module. <a href=":keys">Manage keys</a>', [
        ':keys' => $key_collection_url,
      ]),
      '#tree' => FALSE,
    ];

    $form['s3']['keys']['auth_key_name'] = [
      '#type' => 'key_select',
      '#title' => $this->t('S3 Authentication Key'),
      '#description' => $this->t('Auth key to use AWS S3 client.'),
      '#empty_option' => $this->t('- Select Key -'),
      '#default_value' => $config->get('s3.auth_key_name'),
      '#key_filters' => [
        'provider' => [
          'aws_config',
          'aws_file',
        ],
      ],
      '#states' => [
        'visible' => [
          ':input[name="status"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="status"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $bucketOptions = [
      '' => $this->t('- Select Bucket -'),
    ];
    $bucketRequired = FALSE;
    if ($this->databaseBackup->getS3Client()) {
      $buckets = $this->databaseBackup->getS3Client()->listBuckets();
      foreach ($buckets['Buckets'] as $bucket) {
        $bucketOptions[$bucket['Name']] = $bucket['Name'];
      }
      $bucketRequired = TRUE;
    }
    $form['s3']['bucket'] = [
      '#type' => 'select',
      '#title' => $this->t('S3 Bucket'),
      '#options' => $bucketOptions,
      '#description' => $this->t('Bucket to use when storing the database export file.'),
      '#default_value' => $config->get('s3.bucket'),
      '#states' => [
        'visible' => [
          ':input[name="status"]' => ['checked' => TRUE],
          ':input[name="access_key"]' => ['filled' => TRUE],
          ':input[name="secret_key"]' => ['filled' => TRUE],
        ],
      ],
    ];
    if ($bucketRequired) {
      $form['s3']['bucket']['#states']['required'] = [
        ':input[name="status"]' => ['checked' => TRUE],
      ];
    }

    $form['s3']['folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('S3 Sub-folder'),
      '#description' => $this->t('If you wish to organise your backups into a sub-folder such as /my/subfolder/, enter <i>my/subfolder</i> here without the leading or trailing slashes.'),
      '#default_value' => $config->get('s3.folder'),
    ];

    $regions = [
      '' => $this->t('- Select Region -'),
      'af-south-1' => $this->t('af-south-1'),
      'ap-east-1' => $this->t('ap-east-1'),
      'ap-northeast-1' => $this->t('ap-northeast-1'),
      'ap-northeast-2' => $this->t('ap-northeast-2'),
      'ap-northeast-3' => $this->t('ap-northeast-3'),
      'ap-south-1' => $this->t('ap-south-1'),
      'ap-southeast-1' => $this->t('ap-southeast-1'),
      'ap-southeast-2' => $this->t('ap-southeast-2'),
      'ap-southeast-3' => $this->t('ap-southeast-3'),
      'ca-central-1' => $this->t('ca-central-1'),
      'cn-north-1' => $this->t('cn-north-1'),
      'cn-northwest-1' => $this->t('cn-northwest-1'),
      'eu-central-1' => $this->t('eu-central-1'),
      'eu-north-1' => $this->t('eu-north-1'),
      'eu-south-1' => $this->t('eu-south-1'),
      'eu-west-1' => $this->t('eu-west-1'),
      'eu-west-2' => $this->t('eu-west-2'),
      'eu-west-3' => $this->t('eu-west-3'),
      'me-south-1' => $this->t('me-south-1'),
      'sa-east-1' => $this->t('sa-east-1'),
      'us-east-1' => $this->t('us-east-1'),
      'us-east-2' => $this->t('us-east-2'),
      'us-gov-east-1' => $this->t('us-gov-east-1'),
      'us-gov-west-1' => $this->t('us-gov-west-1'),
      'us-west-1' => $this->t('us-west-1'),
      'us-west-2' => $this->t('us-west-2'),
    ];
    $form['s3']['region'] = [
      '#type' => 'select',
      '#title' => $this->t('S3 Region'),
      '#options' => $regions,
      '#description' => $this->t('Region to use when storing the database export file.'),
      '#default_value' => $config->get('s3.region'),
      '#states' => [
        'visible' => [
          ':input[name="status"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="status"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['options'] = [
      '#type' => 'details',
      '#title' => $this->t('Database Options'),
      '#open' => FALSE,
    ];

    $form['options']['no_data'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('No data'),
      '#description' => $this->t('Do not write any table row information.'),
      '#default_value' => $config->get('settings.no_data'),
    ];

    $form['options']['add_drop_table'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add drop table'),
      '#description' => $this->t('Write a DROP TABLE statement before each CREATE TABLE statement.'),
      '#default_value' => $config->get('settings.add_drop_table'),
    ];

    $form['options']['single_transaction'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Single transaction'),
      '#description' => $this->t('Sets the transaction isolation mode to REPEATABLE READ and sends a START TRANSACTION SQL statement to the server before dumping data.'),
      '#default_value' => $config->get('settings.single_transaction'),
    ];

    $form['options']['lock_tables'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Lock tables'),
      '#description' => $this->t('For each dumped database, lock all tables to be dumped before dumping them.'),
      '#default_value' => $config->get('settings.lock_tables'),
    ];

    $form['options']['add_locks'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add locks'),
      '#description' => $this->t('Surround each table dump with LOCK TABLES and UNLOCK TABLES statements. This results in faster inserts when the dump file is reloaded.'),
      '#default_value' => $config->get('settings.add_locks'),
    ];

    $form['options']['extended_insert'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Extended insert'),
      '#description' => $this->t('Write INSERT statements using multiple-row syntax that includes several VALUES lists. This results in a smaller dump file and speeds up inserts when the file is reloaded.'),
      '#default_value' => $config->get('settings.extended_insert'),
    ];

    $form['options']['complete_insert'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Complete insert'),
      '#description' => $this->t('Use complete INSERT statements that include column names.'),
      '#default_value' => $config->get('settings.complete_insert'),
    ];

    $form['options']['disable_keys'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable keys'),
      '#description' => $this->t('Makes loading the dump file faster because the indexes are created after all rows are inserted.'),
      '#default_value' => $config->get('settings.disable_keys'),
    ];

    $form['options']['no_create_info'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('No create info'),
      '#description' => $this->t('Do not write CREATE TABLE statements that create each dumped table.'),
      '#default_value' => $config->get('settings.no_create_info'),
    ];

    $form['options']['skip_triggers'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Skip triggers'),
      '#description' => $this->t('Include triggers for each dumped table in the output.'),
      '#default_value' => $config->get('settings.skip_triggers'),
    ];

    $form['options']['add_drop_trigger'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add drop trigger'),
      '#description' => $this->t('Write a DROP TRIGGER statement before each CREATE TRIGGER statement.'),
      '#default_value' => $config->get('settings.add_drop_trigger'),
    ];

    $form['options']['routines'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Routines'),
      '#description' => $this->t('Include stored routines (procedures and functions) for the dumped databases in the output.'),
      '#default_value' => $config->get('settings.routines'),
    ];

    $form['options']['hex_blob'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hex blob'),
      '#description' => $this->t('Dump binary columns using hexadecimal notation.'),
      '#default_value' => $config->get('settings.hex_blob'),
    ];

    $form['options']['databases'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Databases'),
      '#description' => $this->t('Treat all name arguments as database names.'),
      '#default_value' => $config->get('settings.databases'),
    ];

    $form['options']['add_drop_database'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add drop database'),
      '#description' => $this->t('Write a DROP DATABASE statement before each CREATE DATABASE statement.'),
      '#default_value' => $config->get('settings.add_drop_database'),
    ];

    $form['options']['skip_tz_utc'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Skip TZ UTC'),
      '#description' => $this->t('This option enables TIMESTAMP columns to be dumped and reloaded between servers in different time zones.'),
      '#default_value' => $config->get('settings.skip_tz_utc'),
    ];

    $form['options']['no_autocommit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('No autocommit'),
      '#description' => $this->t('Please see http://dev.mysql.com/doc/refman/5.7/en/commit.html'),
      '#default_value' => $config->get('settings.no_autocommit'),
    ];

    $form['options']['skip_comments'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Skip comments'),
      '#description' => $this->t('Write additional information in the dump file such as program version, server version, and host.'),
      '#default_value' => $config->get('settings.skip_comments'),
    ];

    $form['options']['skip_dump_date'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Skip dump date'),
      '#description' => $this->t('Produces a date comment at the end of the dump file.'),
      '#default_value' => $config->get('settings.skip_dump_date'),
    ];

    $form['options']['default_character_set'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default character set'),
      '#description' => $this->t('Please see http://dev.mysql.com/doc/refman/5.5/en/charset-unicode-utf8mb4.html'),
      '#default_value' => $config->get('settings.default_character_set'),
    ];

    $form['options']['where'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Where'),
      '#description' => $this->t('Dump only rows selected by the given WHERE condition.'),
      '#default_value' => $config->get('settings.where'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Save Database Backup Config.
    $this->config('s3_db_backup.settings')
      ->set('path', $values['path'])
      ->set('date', $values['date'])
      ->set('settings.compress', $values['compress'])
      ->set('settings.no_data', $values['no_data'])
      ->set('settings.add_drop_table', $values['add_drop_table'])
      ->set('settings.single_transaction', $values['single_transaction'])
      ->set('settings.lock_tables', $values['lock_tables'])
      ->set('settings.add_locks', $values['add_locks'])
      ->set('settings.extended_insert', $values['extended_insert'])
      ->set('settings.complete_insert', $values['complete_insert'])
      ->set('settings.disable_keys', $values['disable_keys'])
      ->set('settings.where', $values['where'])
      ->set('settings.no_create_info', $values['no_create_info'])
      ->set('settings.skip_triggers', $values['skip_triggers'])
      ->set('settings.add_drop_trigger', $values['add_drop_trigger'])
      ->set('settings.routines', $values['routines'])
      ->set('settings.hex_blob', $values['hex_blob'])
      ->set('settings.databases', $values['databases'])
      ->set('settings.add_drop_database', $values['add_drop_database'])
      ->set('settings.skip_tz_utc', $values['skip_tz_utc'])
      ->set('settings.no_autocommit', $values['no_autocommit'])
      ->set('settings.default_character_set', $values['default_character_set'])
      ->set('settings.skip_comments', $values['skip_comments'])
      ->set('settings.skip_dump_date', $values['skip_dump_date'])
      ->save();

    // Save AWS S3 Config.
    $s3Config = $this->config('s3_db_backup.settings');
    $s3Config
      ->set('s3.status', $values['status'])
      ->set('s3.endpoint', $values['endpoint'])
      ->set('s3.bucket', $values['bucket'])
      ->set('s3.folder', $values['folder'])
      ->set('s3.region', $values['region'])
      ->set('s3.auth_key_name', $values['auth_key_name'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}

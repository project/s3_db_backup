<?php

namespace Drupal\s3_db_backup\Controller;

use Aws\S3\Exception\S3Exception;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Form\FormBuilder;
use Drupal\s3_db_backup\S3DatabaseBackup;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * S3 Database Backup Controller.
 */
class S3DatabaseBackupController extends ControllerBase {

  /**
   * S3 Database Backup Service.
   *
   * @var \Drupal\s3_db_backup\S3DatabaseBackup
   */
  protected $databaseBackup;

  /**
   * Date Formatter Service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Form Builder Service.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;

  /**
   * Constructor.
   *
   * @param \Drupal\s3_db_backup\S3DatabaseBackup $databaseBackup
   *   S3 database backup object.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   Date formatter object.
   * @param \Drupal\Core\Form\FormBuilder $formBuilder
   *   Form builder object.
   */
  public function __construct(S3DatabaseBackup $databaseBackup, DateFormatter $dateFormatter, FormBuilder $formBuilder) {
    $this->databaseBackup = $databaseBackup;
    $this->dateFormatter = $dateFormatter;
    $this->formBuilder = $formBuilder;
  }

  /**
   * Service injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container object.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('s3_db_backup'),
      $container->get('date.formatter'),
      $container->get('form_builder')
    );
  }

  /**
   * Backup export history overview.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @return array
   *   Returns render array.
   */
  public function getExportHistory(Request $request): array {
    $config = $this->databaseBackup->getConfig();

    // Build query.
    $query = $this->databaseBackup->getConnection()->select('s3_db_backup', 'e');
    $count_query = clone $query;
    $count_query->addExpression('Count(e.eid)');
    $paged_query = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender');

    // Set Page limit.
    $pagingLimit = 10;
    if ($config->get('page_limit')) {
      $pagingLimit = $config->get('page_limit');
    }
    $paged_query->limit($pagingLimit);
    $paged_query->setCountQuery($count_query);

    // Get export history.
    $results = $paged_query
      ->fields('e', [
        'eid',
        'fid',
        'name',
        'uri',
        's3_object_uri',
        's3_object_key',
        'created',
      ])
      ->orderBy('created', 'DESC')
      ->execute()
      ->fetchAll();

    $rows = [];
    foreach ($results as $result) {
      // Setup data for table.
      // Setup created field.
      $created = $this->dateFormatter->format($result->created, $config->get('date'));

      // Setup file url.
      $fileUrl = '';
      if (!empty($result->uri)) {
        $fileUrl = Link::fromTextAndUrl($this->t('Download'), Url::fromUri(file_create_url($this->databaseBackup->getAdjustedUri($result->uri)), ['attributes' => ['class' => 'button']]));
      }

      // Setup object url.
      $objectUrl = '';
      if (!empty($result->s3_object_key)) {
        $objectUrl = Link::fromTextAndUrl($this->t('Download'), Url::fromRoute('s3_db_backup.download_object', ['objectKey' => $result->s3_object_key], ['attributes' => ['class' => 'button']]));
      }

      // Setup table row.
      $rows[$result->fid] = [
        'eid' => $result->eid,
        'fid' => $result->fid,
        'name' => $result->name,
        'size' => !empty($result->uri) ? sprintf("%4.2f MB", filesize($result->uri) / 1048576) : '',
        'uri' => $fileUrl,
        's3_object_uri' => $objectUrl,
        'created' => $created,
      ];
    }

    // Return export history form.
    return [
      'form' => $this->formBuilder->getForm('Drupal\s3_db_backup\Form\S3DatabaseBackupHistoryForm', $rows),
      'pager' => [
        '#type' => 'pager',
        '#weight' => 5,
      ],
    ];
  }

  /**
   * Download AWS S3 Object.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   * @param string $objectKey
   *   S3 object key.
   *
   * @return \Symfony\Component\HttpFoundation\Response|void
   *   Returns response object if valid, otherwise nothing.
   */
  public function downloadS3Object(Request $request, string $objectKey) {
    $s3 = $this->databaseBackup->getS3Client();
    $config = $this->databaseBackup->getConfig();

    // Get folder to store backup in.
    $objectFolder = '';
    if (!empty($config->get('s3.folder'))) {
      $objectFolder = trim($config->get('s3.folder'));
      if (!empty($objectFolder)) {
        $objectFolder .= '/';
      }
    }

    try {
      // Get the object.
      $result = $s3->getObject([
        'Bucket' => $config->get('s3.bucket'),
        'Key' => $objectFolder . $objectKey,
      ]);

      // Display the object in the browser.
      if ($result) {
        return new Response(
          $result['Body'],
          Response::HTTP_OK,
          ['content-type' => $result['ContentType']]
        );
      }
    }
    catch (S3Exception $e) {
      watchdog_exception('s3_db_backup', $e);
    }
  }

}

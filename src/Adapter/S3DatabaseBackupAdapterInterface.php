<?php

namespace Drupal\s3_db_backup\Adapter;

/**
 * S3 Database Backup Adapter Interface.
 */
interface S3DatabaseBackupAdapterInterface {

  /**
   * Performs the export routine for the adapter.
   *
   * @return bool|int
   *   Returns status or entity id for file export.
   */
  public function export(): int;

}

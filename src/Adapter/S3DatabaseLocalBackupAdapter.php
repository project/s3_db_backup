<?php

namespace Drupal\s3_db_backup\Adapter;

use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\s3_db_backup\S3DatabaseBackup;
use Drupal\s3_db_backup\S3DatabaseBackupClient;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;

/**
 * S3 Database Backup Local Adapter.
 */
class S3DatabaseLocalBackupAdapter implements S3DatabaseBackupAdapterInterface {

  use LoggerChannelTrait;

  /**
   * S3 Database Backup Client Service.
   *
   * @var \Drupal\s3_db_backup\S3DatabaseBackupClient
   */
  protected $databaseBackupClient;

  /**
   * S3 Database Backup Service.
   *
   * @var \Drupal\s3_db_backup\S3DatabaseBackup
   */
  protected $databaseBackup;

  /**
   * Constructor.
   *
   * @param \Drupal\s3_db_backup\S3DatabaseBackupClient $databaseBackupClient
   *   S3 database backup client object.
   * @param \Drupal\s3_db_backup\S3DatabaseBackup $databaseBackup
   *   S3 database backup object.
   */
  public function __construct(S3DatabaseBackupClient $databaseBackupClient, S3DatabaseBackup $databaseBackup) {
    $this->databaseBackupClient = $databaseBackupClient;
    $this->databaseBackup = $databaseBackup;
  }

  /**
   * {@inheritdoc}
   */
  public function export(): int {
    $handler = $this->databaseBackupClient->getFileHandler();
    $handler->setupFile($this->databaseBackupClient->getSettings());

    // Get file and current user.
    $file = $handler->getFile();
    $user = \Drupal::currentUser();

    // Create a file entity.
    $entity = File::create([
      'uri' => $file->getFileUri(),
      'uid' => $user->id(),
      'status' => FileInterface::STATUS_PERMANENT,
    ]);
    $entity->save();

    // Check for file entity id.
    if ($entity->id()) {

      // Insert history entry.
      $this->databaseBackup->addHistoryEntry([
        'fid' => $entity->id(),
        'name' => $file->getFileName(),
        'uri' => $file->getFileUri(),
      ]);

      // Start export.
      $export = $this->databaseBackupClient->dump();
      $export->start($file->getFileUri());
    }
    else {
      $this->getLogger('s3_db_backup')->error('File entity could not be created.');
    }

    // Return file entity id.
    return (int) $entity->id();
  }

}

<?php

namespace Drupal\s3_db_backup\Adapter;

use Drupal\s3_db_backup\S3DatabaseBackup;
use Drupal\s3_db_backup\S3DatabaseBackupClient;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Request;

/**
 * S3 Database Backup Remote Adapter.
 */
class S3DatabaseRemoteBackupAdapter implements S3DatabaseBackupAdapterInterface {

  /**
   * S3 Database Backup Client Service.
   *
   * @var \Drupal\s3_db_backup\S3DatabaseBackupClient
   */
  protected $databaseBackupClient;

  /**
   * S3 Database Backup Service.
   *
   * @var \Drupal\s3_db_backup\S3DatabaseBackup
   */
  protected $databaseBackup;

  /**
   * Constructor.
   *
   * @param \Drupal\s3_db_backup\S3DatabaseBackupClient $databaseBackupClient
   *   S3 database backup client object.
   * @param \Drupal\s3_db_backup\S3DatabaseBackup $databaseBackup
   *   S3 database backup object.
   */
  public function __construct(S3DatabaseBackupClient $databaseBackupClient, S3DatabaseBackup $databaseBackup) {
    $this->databaseBackupClient = $databaseBackupClient;
    $this->databaseBackup = $databaseBackup;
  }

  /**
   * {@inheritdoc}
   */
  public function export(): int {
    $handler = $this->databaseBackupClient->getFileHandler();
    $handler->setupFile($this->databaseBackupClient->getSettings());

    // Get file.
    $file = $handler->getFile();

    // Start export.
    $export = $this->databaseBackupClient->dump();
    $export->start($file->getFileUri());

    // Download export.
    $this->download($file->getFileUri(), [
      'name' => $file->getFileName(),
      'type' => $file->getFileType(),
    ]);

    // Return for consistency.
    return TRUE;
  }

  /**
   * Expose the export for download.
   *
   * @param string $path
   *   Path to export file.
   * @param array $config
   *   Config for response.
   */
  private function download(string $path, array $config) {
    $response = new BinaryFileResponse($path);
    $response->trustXSendfileTypeHeader();
    $response->headers->set('Content-Type', $config['type']);
    $response->setContentDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      $config['name']
    );
    $response->prepare(Request::createFromGlobals());
    $response->send();
  }

}

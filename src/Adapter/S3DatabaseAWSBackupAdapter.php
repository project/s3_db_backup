<?php

namespace Drupal\s3_db_backup\Adapter;

use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\s3_db_backup\S3DatabaseBackup;
use Drupal\s3_db_backup\S3DatabaseBackupClient;
use Aws\S3\Exception\S3Exception;

/**
 * S3 Database Backup AWS Adapter.
 */
class S3DatabaseAWSBackupAdapter implements S3DatabaseBackupAdapterInterface {

  /**
   * S3 Database Backup Client.
   *
   * @var \Drupal\s3_db_backup\S3DatabaseBackupClient
   */
  protected $databaseBackupClient;

  /**
   * S3 Database Backup Service.
   *
   * @var \Drupal\s3_db_backup\S3DatabaseBackup
   */
  protected $databaseBackup;

  /**
   * Constructor.
   *
   * @param \Drupal\s3_db_backup\S3DatabaseBackupClient $databaseBackupClient
   *   S3 database backup client object.
   * @param \Drupal\s3_db_backup\S3DatabaseBackup $databaseBackup
   *   S3 database backup object.
   */
  public function __construct(S3DatabaseBackupClient $databaseBackupClient, S3DatabaseBackup $databaseBackup) {
    $this->databaseBackupClient = $databaseBackupClient;
    $this->databaseBackup = $databaseBackup;
  }

  /**
   * {@inheritdoc}
   */
  public function export(): int {
    $status = FALSE;
    $config = $this->databaseBackup->getConfig();

    // Make sure S3 is enabled.
    if ($config && $config->get('s3.status')) {

      $handler = $this->databaseBackupClient->getFileHandler();
      $handler->setupFile($this->databaseBackupClient->getSettings());

      // Get file.
      if ($file = $handler->getFile()) {

        // Get database export.
        $export = $this->databaseBackupClient->dump();
        $export->start($file->getFileUri());

        // Instantiate an Amazon S3 client.
        $s3 = $this->databaseBackup->getS3Client();

        try {

          // Make sure we have a s3 client.
          if ($s3) {

            // Get folder to store backup in.
            $objectFolder = '';
            if (!empty($config->get('s3.folder'))) {
              $objectFolder = trim($config->get('s3.folder'));
              if (!empty($objectFolder)) {
                $objectFolder .= '/';
              }
            }

            // Upload file to S3.
            // The file size and type are determined by the SDK.
            $objectKey = $file->getFileName();
            $result = $s3->putObject([
              'Bucket' => $config->get('s3.bucket'),
              'Key' => $objectFolder . $objectKey,
              'SourceFile' => $file->getFileUri(),
            ]);

            // Check result.
            if ($result && $objectUrl = $result['ObjectURL']) {

              // Create a file entity.
              $entity = File::create([
                'uri' => $file->getFileUri(),
                'uid' => \Drupal::currentUser()->id(),
                'status' => FileInterface::STATUS_PERMANENT,
              ]);
              $entity->save();

              // Add export history entry.
              $eid = $this->databaseBackup->addHistoryEntry([
                'fid' => $entity->id(),
                'name' => $file->getFileName(),
                'uri' => $file->getFileUri(),
                's3_object_uri' => $objectUrl,
                's3_object_key' => $objectKey,
              ]);
              $status = $eid;
            }
          }
        }
        catch (S3Exception | \Exception $e) {
          watchdog_exception('s3_db_backup', $e);
        }
      }
    }

    return (int) $status;
  }

}

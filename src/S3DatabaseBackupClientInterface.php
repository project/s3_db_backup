<?php

namespace Drupal\s3_db_backup;

/**
 * S3 Database Backup Client Interface.
 */
interface S3DatabaseBackupClientInterface {

  /**
   * Performs the database export.
   */
  public function dump();

}

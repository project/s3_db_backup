<?php

namespace Drupal\s3_db_backup;

use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Drupal\Core\StreamWrapper\PrivateStream;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\file\Entity\File;
use Drupal\key_aws\AWSKeyRepository;

/**
 * S3 Database Backup Service.
 */
class S3DatabaseBackup {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The AWS S3 client.
   *
   * @var \Aws\S3\S3Client
   */
  protected $s3client;

  /**
   * The AWS key repository object.
   *
   * @var \Drupal\key_aws\AWSKeyRepository
   */
  protected $AWSKeyRepository;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection object.
   * @param \Drupal\Core\File\FileSystem $filesystem
   *   Filesystem object.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory object.
   * @param \Drupal\key_aws\AWSKeyRepository $aws_key_repository
   *   AWS Key repository object.
   */
  public function __construct(Connection $connection, FileSystem $filesystem, ConfigFactory $config_factory, AWSKeyRepository $aws_key_repository) {
    $this->connection = $connection;
    $this->fileSystem = $filesystem;
    $this->configFactory = $config_factory;
    $this->AWSKeyRepository = $aws_key_repository;
  }

  /**
   * Get config.
   *
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   *   Returns editable config object.
   */
  public function getConfig() {
    return $this->configFactory->getEditable('s3_db_backup.settings');
  }

  /**
   * Get database connection.
   *
   * @return \Drupal\Core\Database\Connection
   *   Returns database connection.
   */
  public function getConnection(): Connection {
    return $this->connection;
  }

  /**
   * Instantiate and get an Amazon S3 client.
   *
   * @return \Aws\S3\S3Client|bool
   *   Returns S3 client or false if client failed.
   */
  public function getS3Client() {
    // If client not set, let's set one up and store it.
    if (!$this->s3client) {

      // Get config.
      $config = $this->getConfig();

      // Check to see if auth key was set up.
      if (empty($config->get('s3.auth_key_name'))) {
        return FALSE;
      }

      // Get credentials.
      $this->AWSKeyRepository->setKey($config->get('s3.auth_key_name'));
      if (!$this->AWSKeyRepository->getCredentials()) {
        return FALSE;
      }

      // S3 client config.
      $s3ClientConfig = [
        'version' => 'latest',
        'region' => $config->get('s3.region'),
        'credentials' => $this->AWSKeyRepository->getClientCredentials(),
      ];

      // Set endpoint if configured.
      if (!empty($config->get('s3.endpoint'))) {
        $s3ClientConfig['endpoint'] = $config->get('s3.endpoint');
      }

      try {
        // Instantiate an Amazon S3 client.
        $this->s3client = new S3Client($s3ClientConfig);
      }
      catch (S3Exception | \Exception $e) {
        watchdog_exception('s3_db_backup', $e);
        return FALSE;
      }
    }

    return $this->s3client;
  }

  /**
   * Get pre-signed url for object.
   *
   * @param string $objectKey
   *   S3 object key.
   * @param string $expires
   *   S3 expires.
   *
   * @see https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/s3-presigned-url.html
   *
   * @return string|null
   *   Returns S3 pre-signed url.
   */
  public function getPresignedUrl(string $objectKey, string $expires = '+60 minutes'): ?string {
    $presignedUrl = NULL;

    // Get config.
    $config = $this->getConfig();

    // Get client.
    $s3 = $this->getS3Client();

    try {
      // Get object.
      $cmd = $s3->getCommand('GetObject', [
        'Bucket' => $config->get('s3.bucket'),
        'Key' => $objectKey,
      ]);

      // Create pre-signed request.
      $request = $s3->createPresignedRequest($cmd, $expires);
      $presignedUrl = (string) $request->getUri();
    }
    catch (S3Exception | \Exception $e) {
      watchdog_exception('s3_db_backup', $e);
    }

    return $presignedUrl;
  }

  /**
   * Get adjusted uri.
   *
   * @param string $uri
   *   Uri path.
   *
   * @return string
   *   Returns adjusted path.
   */
  public function getAdjustedUri(string $uri): string {
    $scheme = StreamWrapperManager::getScheme($uri);
    $path = $uri;
    if ($scheme == 'private') {
      $private_path = PrivateStream::basePath();
      $path = str_replace('private:/', $private_path, $uri);
    }
    return $path;
  }

  /**
   * Inserts a history entry.
   *
   * @param array $file
   *   Array of file data.
   *
   * @return \Drupal\Core\Database\StatementInterface|int|null
   *   Returns export id.
   *
   * @throws \Exception
   */
  public function addHistoryEntry(array $file) {
    return $this->connection->insert('s3_db_backup')
      ->fields([
        'fid',
        'name',
        'uri',
        's3_object_uri',
        's3_object_key',
        'created',
      ])
      ->values([
        'fid' => $file['fid'],
        'name' => $file['name'],
        'uri' => $file['uri'],
        's3_object_uri' => !empty($file['s3_object_uri']) ? $file['s3_object_uri'] : NULL,
        's3_object_key' => !empty($file['s3_object_key']) ? $file['s3_object_key'] : NULL,
        'created' => \Drupal::time()->getRequestTime(),
      ])
      ->execute();
  }

  /**
   * Load history entry by primary id.
   *
   * @param int $id
   *   Export entry id.
   *
   * @return mixed
   *   Returns export entry object or false.
   */
  public function loadHistoryEntry(int $id) {
    return $this->connection->query('SELECT * FROM {s3_db_backup} WHERE eid = :id', [':id' => $id])
      ->fetchObject();
  }

  /**
   * Load history entry by file id.
   *
   * @param int $fid
   *   Export file entity id.
   *
   * @return mixed
   *   Returns export entry object or false.
   */
  public function loadHistoryEntryByFileId(int $fid) {
    return $this->connection->query('SELECT * FROM {s3_db_backup} WHERE fid = :fid', [':fid' => $fid])
      ->fetchObject();
  }

  /**
   * Remove history entry by file id.
   *
   * @param int $fid
   *   Export file entity id.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteHistoryEntry(int $fid) {
    // Delete database record.
    $this->connection->delete('s3_db_backup')
      ->condition('fid', $fid)
      ->execute();

    // Remove file and managed entry.
    $file = File::load($fid);
    if ($file) {
      $file->delete();
    }
  }

  /**
   * Show/get tables on current database.
   *
   * @return array
   *   Returns an array of database tables.
   */
  public function showTables(): array {
    return $this->connection->query('SHOW TABLES')->fetchAll();
  }

  /**
   * Format results as select list options.
   *
   * @param array $results
   *   Array of results to process.
   *
   * @return array
   *   Returns array of options.
   */
  public function formatOptions(array $results): array {
    $options = [];
    foreach ($results as $result) {
      $value = current((array) $result);
      $options[$value] = $value;
    }
    return $options;
  }

}

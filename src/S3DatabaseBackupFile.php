<?php

namespace Drupal\s3_db_backup;

/**
 * S3 Database Backup File Service.
 */
class S3DatabaseBackupFile {

  /**
   * The file name.
   *
   * @var string
   */
  protected $name;

  /**
   * The file type.
   *
   * @var string
   */
  protected $type;

  /**
   * The file URI.
   *
   * @var string
   */
  protected $uri;

  /**
   * Return the file name.
   *
   * @return string
   *   Returns file name.
   */
  public function getFileName(): string {
    return $this->name;
  }

  /**
   * Set the file name.
   *
   * @param string $name
   *   File name.
   */
  public function setFileName(string $name) {
    $this->name = $name;
  }

  /**
   * Return the file type.
   *
   * @return string
   *   Returns file type.
   */
  public function getFileType(): string {
    return $this->type;
  }

  /**
   * Set the file type.
   *
   * @param string $type
   *   File type.
   */
  public function setFileType(string $type) {
    $this->type = $type;
  }

  /**
   * Return the file uri.
   *
   * @return string
   *   Returns file uri.
   */
  public function getFileUri(): string {
    return $this->uri;
  }

  /**
   * Set the file uri.
   *
   * @param string $uri
   *   File uri.
   */
  public function setFileUri(string $uri) {
    $this->uri = $uri;
  }

}

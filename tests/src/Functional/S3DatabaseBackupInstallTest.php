<?php

namespace Drupal\Tests\s3_db_backup\Functional;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Test the S3 Database Backup module install without errors.
 *
 * @group s3_db_backup
 */
class S3DatabaseBackupInstallTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['s3_db_backup'];

  /**
   * Assert that the s3_db_backup module installed correctly.
   */
  public function testModuleInstalls() {
    // If we get here, then the module was successfully installed during the
    // setUp phase without throwing any Exceptions. Assert that TRUE is true,
    // so at least one assertion runs, and then exit.
    $this->assertTrue(TRUE, 'Module installed correctly.');
  }

}
